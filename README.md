# 腾讯云移动直播终端组件 MLVB SDK
- [移动直播 SDK](https://cloud.tencent.com/product/mlvb)：基于腾讯云 LiteAV 音视频框架实现的一套推拉流和连麦组件，帮您快速上线手机直播功能。
- [云直播](https://cloud.tencent.com/product/lvb)：移动直播 SDK 所依赖的的云端服务，主要提供 RTMP 推流接入点、直播 CDN、实时转码等云端能力。

## SDK 下载
您可以在腾讯云官网下载页面 [DOWNLOAD](https://cloud.tencent.com/document/product/454/7873) 获取精简版、专业版和商用版的 SDK 下载链接，如下为精简版下载链接：

| 所属平台 | Zip下载 | Demo运行说明 | SDK集成指引 | API 列表 |
|:---------:| :--------:|:--------:| :--------:|:--------:|
| iOS | [下载](https://liteavsdk-1252463788.cosgz.myqcloud.com/TXLiteAVSDK_Smart_iOS_latest.zip)| [DOC](https://github.com/tencentyun/MLVBSDK/blob/master/iOS/README.md)| [DOC](https://cloud.tencent.com/document/product/454/7876) | [API](https://cloud.tencent.com/document/product/454/34753) |
| Android | [下载](https://liteavsdk-1252463788.cosgz.myqcloud.com/TXLiteAVSDK_Smart_Android_latest.zip)| [DOC](https://github.com/tencentyun/MLVBSDK/blob/master/Android/README.md)| [DOC](https://cloud.tencent.com/document/product/454/7877) | [API](https://cloud.tencent.com/document/product/454/34766) |
| 小程序 | [下载](https://liteavsdk-1252463788.cosgz.myqcloud.com/MLVB_WXMini_latest.zip)| [DOC](https://github.com/tencentyun/MLVBSDK/blob/master/WXMini/README.md)| [DOC](https://cloud.tencent.com/document/product/454/34925) | [API](https://cloud.tencent.com/document/product/454/15368) |

### Version 8.5 @ 2021.03.18
- iOS & Android：移动直播V2持续优化，包含新特性、易用性、稳定性等，V2更多细节，参考[移动直播-RTC连麦方案](https://cloud.tencent.com/document/product/454/52751);
- iOS & Android：高级美颜效果优化，优化瘦脸、大眼、V脸等相关效果；
- iOS & Android：高级美颜新增窄脸接口；
- iOS & Android：高级美颜人脸特征提取优化；
- iOS & Android：高级美颜新增窄脸接口；
- iOS & Android：直播播放支持0x5 SEI消息类型；
- iOS & Android：优化超级播放器播放部分网络串流seek慢的问题；
- iOS & Android：修复移动直播编码dts异常的问题；
- Android: 修复超级播放器通过fileid方式播放出现报错问题；

## MLVB Demo

![](https://main.qcloudimg.com/raw/ddf1ce540e29f5a43091d9274672e5f1.jpg)

## 小直播 Demo

![](https://main.qcloudimg.com/raw/354d61632bd71d6aec7a833b3afb69bc.jpg)


## 小程序 Demo

![](https://main.qcloudimg.com/raw/913bc2c34495e04dcd3d97eff069df53.jpg)
